import { NgIf } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],

})
export class LoginComponent implements OnInit {
  loginForm: FormGroup | any;
  hide = true;
  constructor(public router: Router, private fb: FormBuilder,) {
    this.loginForm = this.fb.group({
      user: ['', Validators.required],
      pw: ['', Validators.required],

    })
  }
  ngOnInit(): void {

  }
  loginHandler() {
    if (this.loginForm.get('user').value != '' && this.loginForm.get('pw').value != '') {
      localStorage.setItem('user', this.loginForm.get('user').value)
      this.router.navigate(['/home/depotselection'])
    }

  }
}
