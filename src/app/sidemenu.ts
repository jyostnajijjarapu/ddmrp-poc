
export const menuItems = [
    {
        title: 'Dashboard',
        routerLink: 'dashboard',
        icon: 'dashboard'
    },
    {
        title: 'Run',
        routerLink: 'depotselection',
        icon: 'play_arrow',
    },
    {
        title: 'Stock Alerts',
        routerLink: 'alerts',
        icon: 'add_alert',
    },


    {
        title: 'Actions',
        routerLink: 'actions',
        icon: 'sync',
    },

    {
        title: 'Assign',
        routerLink: 'assign',
        icon: 'people_outline',
    },
    {
        title: 'Schedule agreements',
        routerLink: 'schedules',
        icon: 'handshake',
    },
    {
        title: 'Settings',
        routerLink: 'settings',
        icon: 'settings',
    },

]