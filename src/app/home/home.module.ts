import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HomeRoutingModule } from './home-routing.module';
import { DepotSelectionComponent } from './depot-selection/depot-selection.component';
import { MaterialModule } from '../material.module';
import { AlertsComponent } from './alerts/alerts.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ActionsComponent } from './actions/actions.component';
import { AssignComponent } from './assign/assign.component';
import { AgreementsComponent } from './agreements/agreements.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SettingsComponent } from './settings/settings.component';

@NgModule({
  declarations: [
    DepotSelectionComponent,
    AlertsComponent,
    ActionsComponent,
    AssignComponent,
    AgreementsComponent,
    DashboardComponent,
    SettingsComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule, MaterialModule, FormsModule, ReactiveFormsModule,
  ]
})
export class HomeModule { }
