import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DepotSelectionComponent } from './depot-selection.component';

describe('DepotSelectionComponent', () => {
  let component: DepotSelectionComponent;
  let fixture: ComponentFixture<DepotSelectionComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DepotSelectionComponent]
    });
    fixture = TestBed.createComponent(DepotSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
