import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { HttpService } from 'src/app/http.service';

@Component({
  selector: 'app-depot-selection',
  templateUrl: './depot-selection.component.html',
  styleUrls: ['./depot-selection.component.scss']
})
export class DepotSelectionComponent implements OnInit {
  depotCode: any;
  depots: any;
  depotval: any;
  normVal: any;
  repleval: any;
  SLA: any;
  SLB: any;
  SLC: any;
  LTVMDU: any; LTVUKD: any; LTVHYD: any;
  MinLT: any; MaxCappedA: any; MaxCappedB: any; MaxCappedC: any; devCap: any;
  isSavedisabled = true;
  isLoader = false;
  constructor(private httpService: HttpService, private snackBar: MatSnackBar, private router: Router) { }

  ngOnInit(): void {
    this.httpService.get('http://127.0.0.1:5000/connect').subscribe((res: any) => {
      if (res) {
        this.depots = res;
      }
      // console.log(this.depots, "pppppppppppppppppppppp");
    })
  }
  onDepotSelection(event: any) {
    console.log(event)
    this.depotCode = event.value['Depot_Code'];
  }
  onRun() {
    this.isLoader = true;
    let body: any =
    {
      depotCode: this.depotCode,
      normValue: parseInt(this.normVal),
      replenishment: this.repleval,
      serviceLevelA: this.SLA,
      serviceLevelB: this.SLB,
      serviceLevelC: this.SLC,
      // AvgReplenishmentCycleA: this.AvgRCA,
      // AvgReplenishmentCycleB: this.AvgRCB,
      // AvgReplenishmentCycleC: this.AvgRCC,
      // TransitLeadTimeMDU: this.TLTMDU,
      // TransitLeadTimeUKD: this.TLTUKD,
      // TransitLeadTimeHYD: this.TLTHYD,
      LTVariationMDU: this.LTVMDU,
      LTVariationUKD: this.LTVUKD,
      LTVariationHYD: this.LTVHYD,
      MinLTVariation: this.MinLT,
      MaxCappedA: this.MaxCappedA,
      MaxCappedB: this.MaxCappedB,
      MaxCappedC: this.MaxCappedC,
      devCap: this.devCap
    }


    console.log(body, "body")

    this.httpService.post("http://127.0.0.1:5000/run", body).subscribe((res: any) => {
      if (res.result) {
        this.isSavedisabled = false;
        this.isLoader = false;
        console.log(res);
      }
      else {
        this.isLoader = false;
        alert("something went wrong..")
      }
    })
  }
  onSave() {
    this.httpService.get("http://127.0.0.1:5000/save").subscribe((res: any) => {
      if (res.saved == "saved sucessfully") {
        this.snackBar.open("Saved Successfully!", " ", { 'duration': 2000, panelClass: 'blue-snackbar' });
        console.log(res);
        this.router.navigate(['/home/dashboard'])

      }
      else {
        this.snackBar.open("Failed to save", " ", { 'duration': 2000, panelClass: 'red-snackbar' });

      }

    })
  }
}
