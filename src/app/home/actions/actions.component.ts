import { SelectionModel } from '@angular/cdk/collections';
import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
export interface PeriodicElement {
  sku: string;
  id: number;
  desc: string;
  loc: string;
  opt: string;
  assign: string;
  status: string;

}

const ELEMENT_DATA: PeriodicElement[] = [
  { id: 1, sku: '1901', desc: "there is loss of sales expected in depot1", loc: 'Depot 1', opt: '', assign: 'triveni', status: 'Assigned' },
  { id: 2, sku: '1902', desc: "there is loss of sales expected in depot2", loc: 'Depot 2', opt: '', assign: 'likhitha', status: 'UnAssigned' },
  { id: 3, sku: '1919', desc: "there is loss of sales expected in depot3", loc: 'Depot 3', opt: '', assign: 'madhuri', status: 'Assigned' },
  { id: 4, sku: '1920', desc: "there is loss of sales expected in depot1", loc: 'Depot 1', opt: '', assign: 'jyostna', status: 'Assigned' },
  { id: 5, sku: '1908', desc: "there is loss of sales expected in depot1", loc: 'Depot 1', opt: '', assign: 'shashi', status: 'UnAssigned' },
  { id: 6, sku: '1986', desc: "there is loss of sales expected in depot2", loc: 'Depot 2', opt: '', assign: 'sujitha', status: 'Assigned' },
  { id: 7, sku: '1876', desc: "there is loss of sales expected in depot1", loc: 'Depot 1', opt: '', assign: 'devi', status: 'UnAssigned' },
  { id: 8, sku: '2398', desc: "there is loss of sales expected in depot3", loc: 'Depot 3', opt: '', assign: 'swetha', status: 'Assigned' },
  { id: 9, sku: '1245', desc: "there is loss of sales expected in depot2", loc: 'Depot 2', opt: '', assign: 'siva', status: 'UnAssigned' },
  { id: 10, sku: '2678', desc: "there is loss of sales expected in depot2", loc: 'Depot 2', opt: '', assign: 'vamsi', status: 'Assigned' },
];
@Component({
  selector: 'app-actions',
  templateUrl: './actions.component.html',
  styleUrls: ['./actions.component.scss']
})
export class ActionsComponent implements AfterViewInit {
  displayedColumns: string[] = ['select', 'id', 'sku', 'desc', 'loc', 'opt', 'assign', 'status'];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  selection = new SelectionModel<PeriodicElement>(true, []);
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  constructor(private router: Router) { }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }
  toggleAllRows() {
    if (this.isAllSelected()) {
      this.selection.clear();
      return;
    }

    this.selection.select(...this.dataSource.data);
  }

  checkboxLabel(row?: PeriodicElement): string {
    if (!row) {
      return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  next() {
    this.router.navigate(['/home/assign'])
  }
  back() {
    this.router.navigate(['/home/alerts'])
  }
}
