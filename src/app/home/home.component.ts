import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { menuItems } from '../sidemenu';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  isMenuOpen = false;
  username: any;
  menuOptions: any = JSON.parse(JSON.stringify(menuItems));
  constructor(private router: Router,) {

  }
  ngOnInit(): void {
    this.username = localStorage.getItem('user')
  }
  toggleMenu(isOpened?: boolean) {
    if (isOpened) {
      this.isMenuOpen = true;
    } else {
      this.isMenuOpen = !this.isMenuOpen;

    }
  }

  // Make side menu item active when route changes
  applyActiveLinkBasedOnRoute(menuItem: any) {
    return this.router.url.includes(menuItem.routerLink);
  }
  //logout
  loggedout() {
    alert("Are you sure want to logout?")

    this.router.navigate(['/login'])


  }
}
