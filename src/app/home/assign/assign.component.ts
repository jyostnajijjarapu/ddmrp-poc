import { SelectionModel } from '@angular/cdk/collections';
import { Component } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
export interface PeriodicElement {
  id: number;
  repOpt: string;
  lead: string;
  trigger: string;
  cancel: string;


}

const ELEMENT_DATA: PeriodicElement[] = [
  { id: 1, repOpt: 'Perform stock transfer from depot-3', lead: '2 days', trigger: 'Yes', cancel: 'No' },
  { id: 2, repOpt: 'Perform stock transfer from depot-2', lead: '6 days', trigger: 'No', cancel: 'Yes' },
  { id: 3, repOpt: 'Not Possible', lead: '0 days', trigger: 'No', cancel: 'No' },

];
@Component({
  selector: 'app-assign',
  templateUrl: './assign.component.html',
  styleUrls: ['./assign.component.scss']
})
export class AssignComponent {
  displayedColumns: string[] = ['select', 'id', 'repOpt', 'lead', 'trigger', 'cancel'];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  selection = new SelectionModel<PeriodicElement>(true, []);
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
  toggleAllRows() {
    if (this.isAllSelected()) {
      this.selection.clear();
      return;
    }

    this.selection.select(...this.dataSource.data);
  }
  checkboxLabel(row?: PeriodicElement): string {
    if (!row) {
      return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;
  }
}
