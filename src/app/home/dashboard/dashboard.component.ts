import { Component } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent {
  constructor(private sanitizer: DomSanitizer) { }
  reportURL: any = 'https://app.powerbi.com/reportEmbed?reportId=204b27a7-f842-4b03-b277-329e23936335&autoAuth=true&ctid=432a4219-1a46-4b7f-92ce-aae7bc705c26';
  generateSafeImageUrl(): SafeUrl {
    return this.sanitizer.bypassSecurityTrustResourceUrl(this.reportURL);
  }
}
