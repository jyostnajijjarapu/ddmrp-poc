import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home.component';
import { DepotSelectionComponent } from './depot-selection/depot-selection.component';
import { AlertsComponent } from './alerts/alerts.component';
import { ActionsComponent } from './actions/actions.component';
import { AssignComponent } from './assign/assign.component';
import { AgreementsComponent } from './agreements/agreements.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SettingsComponent } from './settings/settings.component';

const routes: Routes = [
  {
    path: '', component: HomeComponent,
    children: [
      { path: 'dashboard', component: DashboardComponent },
      { path: 'depotselection', component: DepotSelectionComponent },
      { path: 'alerts', component: AlertsComponent },
      { path: 'actions', component: ActionsComponent },
      { path: 'assign', component: AssignComponent },
      { path: 'schedules', component: AgreementsComponent },
      { path: 'settings', component: SettingsComponent }
    ]
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
